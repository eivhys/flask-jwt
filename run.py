from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
from sqlalchemy import create_engine

app = Flask(__name__)
db = SQLAlchemy(app)
api = Api(app)

import views
import models
import resources
import os


# Test for todo below
psqlun = os.environ['psqlun']
psqlpw = os.environ['psqlpw']
psqldb = os.environ['psqldb']
if not psqlun or not psqlpw or not psqldb:
    raise Exception(
        'PSQL credentials or database name is missing, make sure environment variables are set!')

# Todo: replace PW, UN and secret with docker system variables
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://{0}:{1}@localhost/{2}'.format(
    psqlun, psqlpw, psqldb)

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'some-secret-string'


@app.before_first_request
def create_tables():
    db.create_all()


app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
jwt = JWTManager(app)

app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return models.RevokedTokenModel.is_jti_blacklisted(jti)


api.add_resource(resources.UserRegistration, '/registration')
api.add_resource(resources.UserLogin, '/login')
api.add_resource(resources.UserLogoutAccess, '/logout/access')
api.add_resource(resources.UserLogoutRefresh, '/logout/refresh')
api.add_resource(resources.TokenRefresh, '/token/refresh')
api.add_resource(resources.AllUsers, '/users')
api.add_resource(resources.SecretResource, '/secret')
